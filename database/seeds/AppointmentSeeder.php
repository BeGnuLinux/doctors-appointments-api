<?php

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return voidsubHours
     */
    public function run()
    {
        Appointment::create([
            'start_date' =>  Carbon::now()->subHours(6),
            'end_date' => Carbon::now()->subHours(5),
            'patient_id' => 1,
            'doctor_id' => 1,
            'status_id' => 1,
        ]);
        Appointment::create([
            'start_date' =>  Carbon::now()->subHours(6),
            'end_date' => Carbon::now()->subHours(5),
            'patient_id' => 2,
            'doctor_id' => 2,
            'status_id' => 1,
        ]);
        Appointment::create([
            'start_date' =>  Carbon::now()->subHours(4),
            'end_date' => Carbon::now()->subHours(3),
            'patient_id' => 3,
            'doctor_id' => 1,
            'status_id' => 1,
        ]);
        Appointment::create([
            'start_date' =>  Carbon::now()->subHours(4),
            'end_date' => Carbon::now()->subHours(3),
            'patient_id' => 4,
            'doctor_id' => 2,
            'status_id' => 1,
        ]);


        Appointment::create([
            'start_date' =>  Carbon::now(),
            'end_date' => Carbon::now()->addHours(1),
            'patient_id' => 1,
            'doctor_id' => 1,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now(),
            'end_date' => Carbon::now()->addHours(1),
            'patient_id' => 2,
            'doctor_id' => 2,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(1),
            'end_date' => Carbon::now()->addHours(2),
            'patient_id' => 3,
            'doctor_id' => 1,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(1),
            'end_date' => Carbon::now()->addHours(2),
            'patient_id' => 4,
            'doctor_id' => 2,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(2),
            'end_date' => Carbon::now()->addHours(3),
            'patient_id' => 5,
            'doctor_id' => 1,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(2),
            'end_date' => Carbon::now()->addHours(3),
            'patient_id' => 6,
            'doctor_id' => 2,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(3),
            'end_date' => Carbon::now()->addHours(4),
            'patient_id' => 7,
            'doctor_id' => 1,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(3),
            'end_date' => Carbon::now()->addHours(4),
            'patient_id' => 8,
            'doctor_id' => 2,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(4),
            'end_date' => Carbon::now()->addHours(5),
            'patient_id' => 9,
            'doctor_id' => 1,
            'status_id' => 1,
        ]);

        Appointment::create([
            'start_date' =>  Carbon::now()->addHours(4),
            'end_date' => Carbon::now()->addHours(5),
            'patient_id' => 10,
            'doctor_id' => 2,
            'status_id' => 1,
        ]);
    }
}
