<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\addTypeRequest;
use App\Http\Requests\User\updateTypeRequest;
use App\Http\Resources\User\TypeCollection;
use App\Http\Resources\User\TypeResource;
use App\Type;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TypeController extends Controller
{
    public function index()
    {
      try{
        $types = Type::latest()->paginate(30);
        return response()->json(['data' => new TypeCollection($types), 'message' => 'Types Retrived.']);
      } catch(Exception $e) {
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function store(addTypeRequest $request)
    {
      $input = $request->validated();
      $input['created_at'] = Carbon::now();

      try{
        Type::create($input);
        return response()->json(['message' => 'Type Created']);
      } catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function show($id)
    {
      $type = Type::find($id);
      
      if($type) {
        return response()->json(['data' => new TypeResource($type), 'message' => 'Type Retrived.']);
      } else {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function update(updateTypeRequest $request, $id)
    {
      $type = Type::find($id);
      $input = $request->validated();
      if($type) {
        $type->updated_at = Carbon::now();
        $type = $type->fill($input);
      }
      try{
        $type->save();
        return response()->json(['data' => new TypeResource($type), 'message' => "Type Updated"]);
      }
      catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function destroy($id)
    {
      $type = Type::find($id);
      try{
        $type->delete();
        return response()->json(['message' => "Type Deleted"]);
      } catch(Exception $e) {
          return response()->json(['message' => $e->getMessage()], 400);
      }
    }
}
