<?php

namespace App\Http\Resources\User;

use App\Http\Resources\User\TypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "userId" =>                  $this->id,
            "userName" =>                $this->name,
            "userEmail" =>               $this->email,
            // "created_at" =>          $this->created_at,
            // "updated_at" =>          $this->updated_at,
            "userType" =>                new TypeResource($this->type),
        ];
    }
}
