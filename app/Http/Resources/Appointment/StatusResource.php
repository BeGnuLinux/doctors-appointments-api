<?php

namespace App\Http\Resources\Appointment;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;

class StatusResource extends JsonResource
{

    public function translateStatus($statusName){
      $statuses = Lang::get('status');
      foreach($statuses as $key => $status) {
        if($statusName == $key) {
          return trans('status.' . $key);
        }
      }
      return $statusName; 
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "statusId" =>               $this->id,
            "statusName" =>             $this->translateStatus($this->name),
            // "created_at" =>          $this->created_at,
            // "updated_at" =>          $this->updated_at,
        ];
    }
}
