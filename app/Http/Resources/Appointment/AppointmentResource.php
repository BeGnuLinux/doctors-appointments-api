<?php

namespace App\Http\Resources\Appointment;

use App\Http\Resources\Patient\PatientResource;
use App\Http\Resources\User\UserResource;
use App\Patient;
use App\Status;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "appointmentId" =>       $this->id,
            "startDate" =>           $this->start_date,
            "endDate" =>             $this->end_date,
            "patient" =>             new PatientResource(Patient::find($this->patient_id)),
            "doctor" =>              new UserResource(User::find($this->doctor_id)),
            "status" =>              new StatusResource(Status::find($this->status_id)),
            // "created_at" =>       $this->created_at,
            // "updated_at" =>       $this->updated_at,
        ];
    }
}
