<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('Content-Language');

        if(!$locale){
            $locale = app()->config->get('app.locale');
        }

        if (!array_key_exists($locale, app()->config->get('app.supported_languages'))) {
            return abort(403, 'Language not supported.');
        }
        app()->setLocale($locale);
        $response = $next($request);
        $response->headers->set('Content-Language', $locale);

        return $response;
    }
}
